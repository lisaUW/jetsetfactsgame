class Game {
    constructor(questions) {
        this.questions = questions;
        this.currentQuestion = 0;
        this.counter = 30;
        this.correct = 0;
        this.incorrect = 0;
        this.unanswered = 0;
        this.timer = 30;
    }
    countdown(){
        console.log(`counter is: ${this.counter}`)
        this.counter--;
        $('#counter').html(this.counter);
        if (this.counter <= 0) {
            console.log("Time's Up!");
            this.timeUp();
        }
    }

    loadQuestion() {
        var game = this;
        this.timer = setInterval(function(){
            game.countdown();
        }, 1000);
        $('#subwrapper').html("<h2> TIME REMAINING <span id='counter'>30</span> SECONDS</h2>")
        $('#subwrapper').append('<h2>' + questions[this.currentQuestion].question + '</h2>');

        for (var i = 0; i < questions[this.currentQuestion].answers.length; i++) {
            // $('#subwrapper').append('<h2>' + questions[this.currentQuestion].question + '</h2>');
            $('#subwrapper').append('<button class="answer-button" id="button-' + i + '" data-name="' + questions[this.currentQuestion].answers[i] + '">' + questions[this.currentQuestion].answers[i] + '</button>');

            /*for (var a = 0; a < questions[i].guesses.length; a++) {
                $('#subwrapper').append('<h2>' + questions[i].guesses + '<h2>')
                $('#subwrapper').append("<button class='answer-button' id='button' data-name='" + questions[this.currentQuestion].answers[i] + '">' +
                    questions[this.currentQuestion].answers[i] + '</button>');
            } */

        }
    }



    nextQuestion() {
        this.counter = 30;
        $('#counter').html(this.counter);
        this.currentQuestion++;
        this.loadQuestion();
    }

    timeUp() {
        clearInterval(this.timer);
        this.unanswered++;
        $('#subwrapper').html('<h2> Out of time!</h2>');
        $('#subwrapper').append('<h3>The Correct Answer Was: ' + questions[this.currentQuestion].correctAnswer + '</h3>')
        var game = this;
        if (this.currentQuestion == questions.length - 1) {
            setTimeout(function(){
                game.results()
            }, 3 * 1000);
        } else {
            setTimeout(function(){
                game.nextQuestion()
            }, 3 * 1000);
            
        }
    }

    reset() {
        clearInterval(this.timer);
        $('#subwrapper').html("<h2>All Done!</h2>");
        $('#subwrapper').append("<h3>Correct: " + this.correct + "</h3>");
        $('#subwrapper').append("<h3>Incorrect: " + this.incorrect + "</h3>");
        $('#subwrapper').append("<h3>Unanswered: " + this.unanswered + "</h3>")
        $('#subwrapper').append("<button id='reset'>RESET</button>");
    }



    clicked(e) {
        clearInterval(this.timer);
        if ($(e.target).data("name") == questions[this.currentQuestion].correctAnswer) {
            this.answeredCorrectly();

        } else {
            this.answeredIncorrectly();
        }

    }
    results() {
        clearInterval(this.timer);
        $('#subwrapper').html("<h2>All Done!</h2>");
        $('#subwrapper').append("<h3>Correct: " + this.correct + "</h3>");
        $('#subwrapper').append("<h3>Incorrect: " + this.incorrect + "</h3>");
        $('#subwrapper').append("<h3>Unanswered: " + this.unanswered + "</h3>")
        $('#subwrapper').append("<button id='reset'> RESET</button>")
    }



    answeredCorrectly() {
        console.log("You Got It!")
        clearInterval(this.timer);
        this.correct++;
        $('#subwrapper').html('<h2>You got it right!</h2>');
        $('#subwrapper').append(questions[this.currentQuestion].image);
        if (this.currentQuestion == questions.length - 1) {
            setTimeout(function(){
                game.results()
            }, 3 * 1000);
        
        } else {
            setTimeout(function(){
                game.nextQuestion()
            }, 3 * 1000);
        }
    }
    answeredIncorrectly() {
        console.log("Sorry! That's not correct!");

        clearInterval(this.timer);
        this.incorrect++;
        $('#subwrapper').html('<h2>You got it wrong!</h2>');
        $('#subwrapper').append('<h3>The Correct Answer Was: ' + questions[this.currentQuestion].correctAnswer + '</h3>')
        if (this.currentQuestion == questions.length - 1) {
            setTimeout(function(){
                game.results()
            }, 3 * 1000);
        } else {
            setTimeout(function(){
                game.nextQuestion()
            }, 3 * 1000);
        }
    }

    reset() {
        this.currentQuestion = 0;
        this.counter = 30;
        this.correct = 0;
        this.incorrect = 0;
        this.unanswered = 0;
        this.loadQuestion();
    }
}




var questions = [{
        question: "What is the capital of Tanzania?",
        answers: ["Dodoma", "Marrakech", "Cape Town", "Nairobi"],
        correctAnswer: "Dodoma",
        image: "<img src='tanzania.jpg'>"
    }, {
        question: "How do you say 'maybe' in French?",
        answers: ["oui", "non", "peut-etre"],
        correctAnswer: "peut-etre",
        image: "<img src='france1.jpg'>"
    },
    {
        question: "What is considered a French delicacy?",
        answers: ["duck", "foie gras", "champagne", "All 3 of these"],
        correctAnswer: "All 3 of these",
        image: "<img src='france2.jpg'>"
    }, {
        question: "What is the capital of Hungary?",
        answers: ["Budapest", "Vienna", "Moscow", "Dubrovnik"],
        correctAnswer: "Budapest",
        image: "<img src='hungary.jpg'>"
    },
    {
        question: "What is the capital of Norway?",
        answers: ["Dublin", "London", "Stockholm", "Oslo"],
        correctAnswer: "Oslo",
        image: "<img src='geiranger.jpg'>"
    }, {
        question: "What is a term in Irish for 'having fun?'",
        answers: ["stick in the mud", "jolly", "good ol' craic", "drunk"],
        correctAnswer: "good ol' craic",
        image: "<img src='craic.jpg'>"
    }
]

let game = new Game(questions);

$('.btn').on('click', function () {
    game.loadQuestion();
})
$(document).on('click', '.answer-button', function (e) {
    game.clicked(e);
})
$(document).on('click', '#reset', function () {
    game.reset();
})


function checkName(e, field) {
    console.log(field.value)
    e.preventDefault()
   if (field.value.length < 3){
        alert("Please enter a name of 3 characters or more")
    } 
}

/*
function initMap() {
    console.log('initMap')
var mapOptions = {
    zoom: 13,
    center: new google.maps.LatLng(51.5,-0.11)
  }
  
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  console.log(`map is ${map}`)
}
*/

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// 
function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    });
    console.log('map is ' + map);
    // Create the search box and link it to the UI element.
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
      searchBox.setBounds(map.getBounds());
    });
    let markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();
  
      if (places.length == 0) {
        return;
      }
      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];
      // For each place, get the icon, name and location.
      const bounds = new google.maps.LatLngBounds();
      places.forEach((place) => {
        if (!place.geometry || !place.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
        const icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25),
        };
        // Create a marker for each place.
        markers.push(
          new google.maps.Marker({
            map,
            icon,
            title: place.name,
            position: place.geometry.location,
          })
        );
  
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }
  